use std::iter::Peekable;

use logos::Logos;
use rowan::GreenNodeBuilder;

#[derive(Logos, Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(u16)]
enum SyntaxKind {
    #[token(".")]
    Period,

    #[token("const")]
    ConstKw,

    #[token("=")]
    Equals,

    #[token(",")]
    Comma,

    #[token(";")]
    Semicolon,

    #[token("var")]
    VarKw,

    #[token("procedure")]
    ProcedureKw,

    #[token(":=")]
    ColonEquals,

    #[token("call")]
    CallKw,

    #[token("?")]
    QuestionMark,

    #[token("!")]
    Bang,

    #[token("begin")]
    BeginKw,

    #[token("end")]
    EndKw,

    #[token("if")]
    IfKw,

    #[token("then")]
    ThenKw,

    #[token("while")]
    WhileKw,

    #[token("do")]
    DoKw,

    #[token("odd")]
    OddKw,

    #[token("#")]
    Hash,

    #[token("<")]
    Less,

    #[token("<=")]
    LessEquals,

    #[token(">")]
    Greater,

    #[token(">=")]
    GreaterEquals,

    #[token("+")]
    Plus,

    #[token("-")]
    Minus,

    #[token("*")]
    Asterisk,

    #[token("/")]
    Slash,

    #[token("(")]
    LeftParen,

    #[token(")")]
    RightParen,

    #[regex(r"[a-zA-Z_][a-zA-Z0-9_]*")]
    Ident,

    #[regex(r"[0-9]+")]
    Number,

    #[regex(r"[ \t\r\n]+")]
    Whitespace,

    #[error]
    Error,

    Root,
    ConstBlock,
    VarBlock,
    ProcedureBlock,
    AssignmentStatement,
    CallStatement,
    InputStatement,
    OutputStatement,
    IfStatement,
    AggregateStatement,
    WhileStatement,
    OddCondition,
    ComparisonCondition,
    Expression,
    SignedExpression,
    Term,
    Factor,
}

pub(crate) fn compile(source: &str, output_path: &str) {
    let tokens = lex(source);
    let p = parse(tokens);
}

struct Lexer<'a>(logos::Lexer<'a, SyntaxKind>);

impl<'a> Iterator for Lexer<'a> {
    type Item = (SyntaxKind, &'a str);

    fn next(&mut self) -> Option<Self::Item> {
        let tok = self.0.next()?;
        let lit = self.0.slice();
        Some((tok, lit))
    }
}

fn lex(source: &str) -> Peekable<Lexer<'_>> {
    Lexer(SyntaxKind::lexer(source)).peekable()
}

impl From<SyntaxKind> for rowan::SyntaxKind {
    fn from(t: SyntaxKind) -> Self {
        Self(t as u16)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum PlZero {}

impl rowan::Language for PlZero {
    type Kind = SyntaxKind;
    fn kind_from_raw(raw: rowan::SyntaxKind) -> Self::Kind {
        assert!(raw.0 <= SyntaxKind::Factor as u16);
        unsafe { std::mem::transmute::<u16, SyntaxKind>(raw.0) }
    }

    fn kind_to_raw(kind: Self::Kind) -> rowan::SyntaxKind {
        kind.into()
    }
}

struct Parse {
    green_node: rowan::GreenNode,
    errors: Vec<String>,
}

fn parse(tokens: Peekable<Lexer<'_>>) -> Parse {
    struct Parser<'a> {
        tokens: Peekable<Lexer<'a>>,
        builder: GreenNodeBuilder<'static>,
        errors: Vec<String>,
    }

    impl<'a> Parser<'a> {
        fn parse(mut self) -> Parse {
            self.builder.start_node(SyntaxKind::Root.into());
            self.block();
            self.skip_ws();
            assert_eq!(self.current(), Some(SyntaxKind::Period));
            self.builder.finish_node();
            Parse {
                green_node: self.builder.finish(),
                errors: self.errors,
            }
        }

        fn block(&mut self) {
            self.skip_ws();
            if let Some(SyntaxKind::ConstKw) = self.current() {
                self.builder.start_node(SyntaxKind::ConstBlock.into());
                self.bump();
                loop {
                    self.skip_ws();
                    assert_eq!(self.current(), Some(SyntaxKind::Ident));
                    self.bump();
                    self.skip_ws();
                    assert_eq!(self.current(), Some(SyntaxKind::Equals));
                    self.bump();
                    self.skip_ws();
                    assert_eq!(self.current(), Some(SyntaxKind::Number));
                    self.bump();
                    self.skip_ws();
                    match self.current() {
                        Some(SyntaxKind::Comma) => {
                            self.bump();
                        }
                        Some(SyntaxKind::Semicolon) => break,
                        _ => todo!(),
                    }
                }
                self.bump();
                self.builder.finish_node();
            }

            self.skip_ws();
            if let Some(SyntaxKind::VarKw) = self.current() {
                self.builder.start_node(SyntaxKind::VarBlock.into());
                self.bump();
                loop {
                    self.skip_ws();
                    assert_eq!(self.current(), Some(SyntaxKind::Ident));
                    self.bump();
                    self.skip_ws();
                    match self.current() {
                        Some(SyntaxKind::Comma) => {
                            self.bump();
                        }
                        Some(SyntaxKind::Semicolon) => break,
                        _ => todo!(),
                    }
                }
                self.bump();
                self.builder.finish_node();
            }

            self.skip_ws();
            while let Some(SyntaxKind::ProcedureKw) = self.current() {
                self.builder.start_node(SyntaxKind::ProcedureBlock.into());
                self.bump();
                assert_eq!(self.current(), Some(SyntaxKind::Ident));
                self.bump();
                assert_eq!(self.current(), Some(SyntaxKind::Semicolon));
                self.bump();
                self.block();
                assert_eq!(self.current(), Some(SyntaxKind::Semicolon));
                self.bump();

                self.skip_ws();
            }

            self.statement();
        }

        fn statement(&mut self) {
            self.skip_ws();
            match self.current() {
                Some(SyntaxKind::Period) => {}
                Some(SyntaxKind::Ident) => {
                    self.builder
                        .start_node(SyntaxKind::AssignmentStatement.into());
                    self.bump();
                    self.skip_ws();
                    assert_eq!(self.current(), Some(SyntaxKind::ColonEquals));
                    self.bump();
                    self.skip_ws();
                    self.expression();
                    self.builder.finish_node();
                }
                Some(SyntaxKind::CallKw) => {
                    self.builder.start_node(SyntaxKind::CallStatement.into());
                    self.bump();
                    self.skip_ws();
                    assert_eq!(self.current(), Some(SyntaxKind::Ident));
                    self.bump();
                    self.builder.finish_node();
                }
                Some(SyntaxKind::QuestionMark) => {
                    self.builder.start_node(SyntaxKind::InputStatement.into());
                    self.bump();
                    self.skip_ws();
                    assert_eq!(self.current(), Some(SyntaxKind::Ident));
                    self.bump();
                    self.builder.finish_node();
                }
                Some(SyntaxKind::Bang) => {
                    self.builder.start_node(SyntaxKind::OutputStatement.into());
                    self.bump();
                    self.skip_ws();
                    assert_eq!(self.current(), Some(SyntaxKind::Ident));
                    self.bump();
                    self.builder.finish_node();
                }
                Some(SyntaxKind::BeginKw) => {
                    self.builder
                        .start_node(SyntaxKind::AggregateStatement.into());
                    self.bump();
                    self.statement();
                    self.skip_ws();
                    while let Some(SyntaxKind::Semicolon) = self.current() {
                        self.bump();
                        self.statement();
                        self.skip_ws();
                    }
                    assert_eq!(self.current(), Some(SyntaxKind::EndKw));
                    self.bump();
                    self.builder.finish_node();
                }
                Some(SyntaxKind::IfKw) => {
                    self.builder.start_node(SyntaxKind::IfStatement.into());
                    self.bump();
                    self.condition();
                    self.skip_ws();
                    assert_eq!(self.current(), Some(SyntaxKind::ThenKw));
                    self.bump();
                    self.statement();
                    self.builder.finish_node();
                }
                Some(SyntaxKind::WhileKw) => {
                    self.builder.start_node(SyntaxKind::WhileStatement.into());
                    self.bump();
                    self.condition();
                    self.skip_ws();
                    assert_eq!(self.current(), Some(SyntaxKind::DoKw));
                    self.bump();
                    self.statement();
                    self.builder.finish_node();
                }
                _ => todo!(),
            }
        }

        fn condition(&mut self) {
            self.skip_ws();
            match self.current() {
                Some(SyntaxKind::OddKw) => {
                    self.builder.start_node(SyntaxKind::OddCondition.into());
                    self.bump();
                    self.expression();
                    self.builder.finish_node();
                }
                _ => {
                    self.builder
                        .start_node(SyntaxKind::ComparisonCondition.into());
                    self.expression();
                    match self.current() {
                        Some(
                            SyntaxKind::Equals
                            | SyntaxKind::Hash
                            | SyntaxKind::Less
                            | SyntaxKind::LessEquals
                            | SyntaxKind::Greater
                            | SyntaxKind::GreaterEquals,
                        ) => {
                            self.bump();
                            self.expression();
                            self.builder.finish_node();
                        }
                        _ => todo!(),
                    }
                }
            }
        }

        fn expression(&mut self) {
            self.skip_ws();
            match self.current() {
                Some(SyntaxKind::Plus | SyntaxKind::Minus) => {
                    self.builder.start_node(SyntaxKind::SignedExpression.into());
                    self.bump();
                }
                _ => {
                    self.builder.start_node(SyntaxKind::Expression.into());
                }
            }
            self.term();
            self.skip_ws();
            while let Some(SyntaxKind::Plus | SyntaxKind::Minus) = self.current() {
                self.bump();
                self.term();
                self.skip_ws();
            }
            self.builder.finish_node();
        }

        fn term(&mut self) {
            self.builder.start_node(SyntaxKind::Term.into());
            self.factor();
            self.skip_ws();
            while let Some(SyntaxKind::Asterisk | SyntaxKind::Slash) = self.current() {
                self.bump();
                self.factor();
                self.skip_ws();
            }
            self.builder.finish_node();
        }

        fn factor(&mut self) {
            self.builder.start_node(SyntaxKind::Factor.into());
            match self.current() {
                Some(SyntaxKind::Ident | SyntaxKind::Number) => {
                    self.bump();
                }
                Some(SyntaxKind::LeftParen) => {
                    self.bump();
                    self.expression();
                    assert_eq!(self.current(), Some(SyntaxKind::RightParen));
                    self.bump();
                }
                _ => todo!(),
            }
            self.builder.finish_node();
        }

        fn bump(&mut self) {
            let (kind, text) = self.tokens.next().unwrap();
            self.builder.token(kind.into(), text);
        }

        fn current(&mut self) -> Option<SyntaxKind> {
            self.tokens.peek().map(|(kind, _)| *kind)
        }

        fn skip_ws(&mut self) {
            while self.current() == Some(SyntaxKind::Whitespace) {
                self.bump();
            }
        }
    }

    Parser {
        tokens,
        builder: GreenNodeBuilder::new(),
        errors: Vec::new(),
    }
    .parse()
}

type SyntaxNode = rowan::SyntaxNode<PlZero>;

impl Parse {
    fn syntax(&self) -> SyntaxNode {
        SyntaxNode::new_root(self.green_node.clone())
    }
}

#[cfg(test)]
mod tests {
    use expect_test::{expect, Expect};

    use super::*;

    fn check(input: &str, expected: Expect) {
        expected.assert_eq(&format!("{:?}", parse(lex(input)).syntax()));
    }

    #[test]
    fn test_empty_program() {
        check(".", expect![[r#"Root@0..0"#]])
    }

    #[test]
    fn test_simple_program() {
        check(
            "
            var x, squ;

            procedure square;
            begin
                squ := x * 
            end;

            begin
                x := 1;
                while x <= 10 do
                begin
                    call square;
                    ! squ;
                    x := x + 1
                end
            end.
        ",
            expect![[]],
        )
    }
}
