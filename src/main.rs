use crate::compile::compile;
use std::{fs, path::PathBuf};

use clap::{App, Arg, SubCommand};

mod compile;

fn main() {
    let app = App::new("pl0x").subcommand(
        SubCommand::with_name("compile")
            .arg(Arg::with_name("output").short("o").long("output"))
            .arg(Arg::with_name("FILE").required(true).index(1)),
    );
    let matches = app.get_matches();

    let (subcommand, subcommand_matches) = matches.subcommand();

    match subcommand {
        "compile" => {
            let filename = subcommand_matches
                .as_ref()
                .unwrap()
                .value_of("FILE")
                .unwrap();
            let contents = String::from_utf8_lossy(&fs::read(filename).unwrap()).to_string();
            let output_path = match subcommand_matches.as_ref().unwrap().value_of("output") {
                Some(p) => p.to_string(),
                None => format!(
                    "{}.exe",
                    PathBuf::from(filename)
                        .file_stem()
                        .unwrap()
                        .to_string_lossy()
                ),
            };
            compile(&contents, &output_path);
        }
        _ => unreachable!(),
    }
}
